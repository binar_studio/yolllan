//= require js-new-homepage
//= require functions
//= require jquery-ui
//= require jquery.easing
//= require jquery.themepunch.revolution.min
//= require jquery.nicescroll.min
//= require jquery.carouFredSel-6.2.1-packed
//= require helper-plugins/jquery.touchSwipe.min
//= require helper-plugins/jquery.mousewheel.min
//= require helper-plugins/jquery.transit.min
//= require helper-plugins/jquery.ba-throttle-debounce.min
//= require jquery.customSelect
//= require bootstrap.min