ActiveAdmin.register RoomType do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  permit_params :name, :hotel, photos_attributes: [:id, :path, :_destroy]

  form html: { multipart: true } do |f|
    f.inputs "Room Type" do
      f.input :name
      f.input :hotel
    end
    f.has_many :photos do |ff|
      ff.input :path, :as => :file, :hint => image_tag(ff.object.path_url(:thumb)) if ff.object.path
      ff.input :_destroy, :as=>:boolean, :required => false, :label=>'Remove'

    end
    f.actions
  end

end
