class Hotel < ActiveRecord::Base

  has_many :photos, as: :imageable
  has_many :rooms
  has_and_belongs_to_many :locations

  accepts_nested_attributes_for :photos, :allow_destroy => true

end