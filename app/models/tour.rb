class Tour < ActiveRecord::Base

  has_many :photos, as: :imageable
  has_many :tour_activities
  has_many :locations, through: :tour_activities

  accepts_nested_attributes_for :photos, :allow_destroy => true

end
