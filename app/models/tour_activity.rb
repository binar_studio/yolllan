class TourActivity < ActiveRecord::Base

  belongs_to :tour
  has_and_belongs_to_many :locations
  has_many :photos, as: :imageable

  accepts_nested_attributes_for :photos, :allow_destroy => true
end
