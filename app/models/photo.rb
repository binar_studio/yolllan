class Photo < ActiveRecord::Base

  belongs_to :imageable, polymorphic: true

  mount_uploader :path, PhotoUploader
end
