class RoomType < ActiveRecord::Base

  belongs_to :hotel
  has_many :rooms
  has_many :photos, as: :imageable

  accepts_nested_attributes_for :photos, :allow_destroy => true

end
