class CreateTourActivities < ActiveRecord::Migration
  def change
    create_table :tour_activities do |t|
      t.string :name
      t.belongs_to :tour, index: true

      t.timestamps null: false
    end
    create_join_table :tour_activities, :locations
  end
end
