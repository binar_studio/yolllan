class AddCapacitiesToRooms < ActiveRecord::Migration
  def change

    change_table :rooms do |t|
      t.integer :adult_max
      t.integer :child_max
      t.remove :capacity
    end

  end
end
