class AddRoomTypeToRooms < ActiveRecord::Migration
  def change

    change_table :rooms do |t|
      t.belongs_to :room_type, index: true
    end

  end
end
