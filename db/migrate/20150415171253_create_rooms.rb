class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :name
      t.belongs_to :hotel, index: true
      t.integer :capacity

      t.timestamps null: false
    end
  end
end
