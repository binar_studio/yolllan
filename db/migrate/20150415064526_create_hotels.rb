class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.string :name
      t.text :description
      t.integer :star
      t.string :address
      t.string :phone
      t.string :email

      t.timestamps null: false
    end
    create_join_table :hotels, :locations
  end
end
