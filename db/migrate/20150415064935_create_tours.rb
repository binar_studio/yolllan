class CreateTours < ActiveRecord::Migration
  def change
    create_table :tours do |t|
      t.string :name
      t.datetime :start_date
      t.datetime :end_date
      t.integer :capacity
      t.integer :available

      t.timestamps null: false
    end
  end
end
