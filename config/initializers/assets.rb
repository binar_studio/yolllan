# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile +=
    %w(
        homepage.css
        html5shiv.js
        respond.min.js
        font-awesome-ie7.css
        homepage.js
        homepage.end.js

        pages.css
        pages.js
        pages.end.js

        hotels.search.css
        hotels.search.js
        hotels.hotel.css
        hotels.hotel.js

        tours.search.css
        tours.search.js
        tours.tour.css
        tours.tour.js
  )